#!/usr/bin/env python3
#
# A simple tool for building DIY batteries
# 
# Written by Danila Gornushko a.k.a. okhsunrog in 2022
# email: to@okhsunrog.ru
# Published under The MIT License (MIT)
import csv
import itertools


def par_res(inlst):
    res = 0.0
    for i in inlst:
        res += 1/get_value(i)
    return 1/res


def print_data(series):
    write_res_to_csv = input("Save result to csv? (y/N) ").lower() == "y"
    if write_res_to_csv:
        csv_res_name = input("Enter result file name: ")
        res_file = open(csv_res_name, "w")

    print("After distribution: ")
    all_res = []
    for i in range(len(series)):
        out_str = "Series " + str(i + 1) + ": "
        print(out_str)
        if write_res_to_csv:
            res_file.write(out_str + "\n")
        for y in series[i]:
            out_str = names[y] + ": " + str(get_value(y))
            print(out_str)
            if write_res_to_csv:
                res_file.write(out_str.replace(":", ",") + "\n")
        x = par_res(series[i])
        all_res.append(x)
        out_str = "Series resistance: {0:.5f}".format(x)
        print(out_str + "\n")
        if write_res_to_csv:
            res_file.write(out_str.replace(":", ",") + "\n\n")
    max1 = max(all_res)
    min1 = min(all_res)
    out_str = "Minimum series resistance: {0:.5f}".format(min1)
    out_str1 = "Maximum series resistance: {0:.5f}".format(max1) 
    out_str2 = "The range of series resistances: {0:.3f}%".format((max1 - min1) / (max1 / 100))
    print(out_str)
    print(out_str1)
    print(out_str2)
    if write_res_to_csv:
        res_file.write(out_str.replace(":", ",") + "\n")
        res_file.write(out_str1.replace(":", ",") + "\n")
        res_file.write(out_str2.replace(":", ",") + "\n")
        res_file.close()


def print_sorted(arr):
    print("Sorted cells: ")
    c = 1
    for i in arr:
        print("{}.     {}: {}".format(c, names[i], get_value(i)))
        c += 1
    print("")


def get_value(val):
    return data[val]


def sort_cells(list_i, p):
    series = []
    while list_i:
        temp = []
        if p > 1:
            for i in range(p - 1):
                temp.append(list_i.pop((len(list_i) // (p - 1)) * i))
        temp.append(list_i.pop())
        series.append(temp)
    return series


def factor(n):
    res = []
    d = 2
    while d * d <= n:
        if n % d == 0:
            res.append(d)
            n //= d
        else:
            d += 1
    if n > 1:
        res.append(n)
    return res


with open('battery.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    data_str = list(csv_reader)
data = []
names = []
for i in data_str:
    try:
        data.append(float(i[1]))
        names.append(str(i[0]))
    except ValueError:
        break
sorted_data = sorted(range(len(data)), key=get_value)
print_sorted(sorted_data)
min0 = min(data)
max0 = max(data)
print("Minimum cell resistance: {0:.2f}".format(min0))
print("Maximum cell resistance: {0:.2f}".format(max0))
print("The range of cell resistances: {0:.3f}%".format((max0 - min0) / (max0 / 100)))


ser = int(input("Enter number of Series (S): "))
par = int(input("Enter number of Parallel (P): "))
if par > 0 and ser > 0:
    print("Calculating {}s{}p battery...".format(ser, par))
else:
    exit(0)
print("")

v = len(data) // par
if v < ser:
    print("Not enough of cells")
    exit(0)
v = par * ser
print("Using the first {} cells from the list above".format(v))

sorted_data = sorted_data[:v:]
max22 = get_value(sorted_data[-1])
print("Maximum resistance among the cells used: {0:.2f}".format(max22))
print("The final range of cell resistances: {0:.3f}%\n".format(((max22 - min0) / (max22 / 100))))

f = factor(par)
res = sorted_data
for i in f:
    res = sort_cells(res, i)

while type(res[0][0]) is not int:
    tmp = []
    for x in res:
        tmp.append(list(itertools.chain(*x)))
    res = tmp

print_data(res)

