# pybatcalc

A simple tool for building DIY batteries.
I wrote the program when I was building a battery for my electric skateboard. Using it prevents cell from imbalance and prolongs the battery life. However, I still strongly recommend using a balance BMS.

## How to use

1. Create a table with 2 columns (for example use LibreOffice Calc). In first column put names of cells (can be any string, you probably want to use numbers here or numbers with any text prefixes), in second column put resistances of the cells.

![Table example](/img/1.png)

2. Save the table as an svg file (use comma as a delimiter) with name `battery.svg`. You can actually omit the first step and create an svg file in a text editor if you know what you're doing.

3. Download or clone the repo, replace the default `battery.svg` file, given as an example with your file. You can take a look at the file if the structure is not quite clear for you. It contains some values of my 21700 Li-Ion cells.

4. Make sure you have Python 3 installed and the `battery.svg` and `pybatcalc.py` files are in the same directory. Run `pybatcalc.py`.  You can use your favorite IDE (PyCharm, for example) or just run it from console/terminal. If you're on Linux/MacOS, use following command (don't forget to navigate to the directory where the code is):

```./pybatcalc.py```

or

```python pybatcalc.py```

or

```python3 pybatcalc.py```

You may need to make the file executable (`chmod +x pybatcalc.py`).

If you're on Windows:

Open Command Promt. Navigate to the directory where the code is located. Run:

```python pybatcalc.py```

Or just use PyCharm.

5. Enter the parameters of the battery you want to build:
![Run code](/img/2.png)
![Enter values](/img/3.png)

6. Copy the result and save it to some text file (hint: use Ctr+Shift+C to copy from Linux console).
The most important values is **The range of series resistances**. The lower the value the better. I have 0.2% for my 10s6p battery and 0.6% for my 12s3p battery. Now you know what cell fits better in each parallel.

![Result](/img/4.png)


## Important note
I recommend measuring resistance using an accurate device. I use YR1035 and the values are quite accurate. Make sure all cells have the same or similar voltage before measuring.
